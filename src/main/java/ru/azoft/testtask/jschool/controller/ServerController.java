package ru.azoft.testtask.jschool.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.azoft.testtask.jschool.entity.User;
import ru.azoft.testtask.jschool.service.UserService;

/**
 * Created by Admin on 29.05.2017.
 */

@Controller

public class ServerController {

    @Autowired
    private UserService userService;

    // выводим главную страницу index.jsp

    @RequestMapping(value = "/jschool", method = RequestMethod.GET)
    public String mainPage(Model model) {   return userService.getAllUsers(model);  }

    // выводим страницу добавления нового пользователя

    @RequestMapping(value = "/jschool/add", method = RequestMethod.GET)
    public String getNewUserPage(Model model) {   return userService.getAddPage(model);  }

    // сохраняем пользователя и выводим страницу успешного добавления

    @RequestMapping(value = "/jschool/add", method = RequestMethod.POST)
    public String saveNewUser(@ModelAttribute("userAttribute") User user) {   return userService.saveNewUser(user);  }

    // выводим страницу изменения пользователя

    @RequestMapping(value = "/jschool/edit/{id}", method = RequestMethod.GET)
    public String getEditUserPage(@PathVariable("id") long id, Model model) {   return userService.getEditPage(id, model);  }

    // сохраняем пользователя и выводим страницу успешного сохранения

    @RequestMapping(value = "/jschool/edit/{id}", method = RequestMethod.POST)
    public String saveEditedUser(@ModelAttribute("userAttribute") User user) {   return userService.saveUser(user);  }

    // выводим страницу изменения пароля пользователя

    @RequestMapping(value = "/jschool/changepsw/{id}", method = RequestMethod.GET)
    public String getChangePasswordPage(@PathVariable("id") long id, Model model) {   return userService.getChangePswPage(id, model);  }

    // сохраняем пароль и выводим страницу успешного сохранения

    @RequestMapping(value = "/jschool/changepsw/{id}", method = RequestMethod.POST)
    public String saveNewPassword(@ModelAttribute("userAttribute") User user) {   return userService.savePassword(user);  }

    // выводим страницу запроса на удаление пользователя

    @RequestMapping(value = "/jschool/delete/{id}", method = RequestMethod.GET)
    public String getDeleteUserPage (@PathVariable("id") long id, Model model) {  return userService.getDeletePage(id, model);  }

    // удаляем пользователя и выводим страницу успешного удаления

    @RequestMapping(value = "/jschool/deleteuser/{id}", method = RequestMethod.GET)
    public String deleteUser (@ModelAttribute("userAttribute") User user) {  return userService.delUser(user);  }

}
