package ru.azoft.testtask.jschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.azoft.testtask.jschool.entity.User;

/**
 * Created by Admin on 30.05.2017.
 */

public interface UserRepository extends JpaRepository<User, Long> {

}
