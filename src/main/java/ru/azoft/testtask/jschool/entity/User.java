package ru.azoft.testtask.jschool.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Admin on 30.05.2017.
 */

@Entity
@Table(name = "users") // название таблицы

public class User implements Serializable{

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "lastname", nullable = false)
    private String lastname;

    // Для удобства будем работать с датой как со строкой
    @Column(name = "birthdate", nullable = false)
    private String birthdate;

    @Column(name = "login", nullable = false)
    private String login;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "address", nullable = false)
    private String address;

    public User() {  }

    public User(String name, String lastname, String birthdate, String login, String password, String description, String address) {
        this.name = name;
        this.lastname = lastname;
        this.birthdate = birthdate;
        this.login = login;
        this.password = password;
        this.description = description;
        this.address = address;
    }

    public String getName() { return name; }

    public String getLastname() {  return lastname;  }

    public String getBirthdate() { return birthdate;  }

    public String getLogin() { return login; }

    public String getPassword() { return password; }

    public String getDescription() { return description; }

    public String getAddress() { return address; }

    public void setName(String name) { this.name = name; }

    public void setLastname(String lastname) { this.lastname = lastname; }

    public void setBirthdate(String birthdate) { this.birthdate = birthdate; }

    public void setLogin(String login) { this.login = login; }

    public void setPassword(String password) {  this.password = password; }

    public void setDescription(String description) { this.description = description;  }

    public void setAddress(String address) { this.address = address;  }

    public long getId() { return id;  }

    public void setId(long id) {   this.id = id;   }

}


