package ru.azoft.testtask.jschool.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import ru.azoft.testtask.jschool.entity.User;
import ru.azoft.testtask.jschool.repository.UserRepository;

import java.util.List;

/**
 * Created by Admin on 30.05.2017.
 */

@Service
public class UserServiceImpl implements UserService{

   @Autowired
   private UserRepository repository;


   public String getAllUsers (Model model) {

       // Создаем коллекцию List с элементами типа (класса) User
      List<User> users;

       // Получаем всех пользователей из БД через репозиторий Spring
      users = repository.findAll();

       // Передаем в модель кол-во пользователей и саму коллекцию
      model.addAttribute("count", users.size());
      model.addAttribute("users", users);

   return "index";
}


    public String getAddPage(Model model) {

        // Создаем новый экземпляр класса User и передаем в модель
        model.addAttribute("userAttribute", new User());

        // Выводим страницу добавления пользователя
        return "add";
    }


    public String saveNewUser(User user) {

        try {

            repository.save(user); // Добавляем пользователя через репозиторий Spring

            return "added"; // Выводим страницу успещного добавления пользователя
        }
        catch (Exception e) // Обрабатываем универсальное исключение
        {
            return "error"; // Выводим страницу с ошибкой
        }
    }


    public String getEditPage(long id, Model model) {

        // Получаем пользователя по id и передаем в модель
        model.addAttribute("userAttribute",getUserById(id));

        // Выводим страницу редактирования пользователя
        return "edit";
    }


    public String saveUser(User user) {

        // Сохраняем пользователя

        try {
            repository.saveAndFlush(user); // Сохраняем пользователя через репозиторий Spring

            return "saved"; // Выводим страницу успещного сохранения
        }
        catch (Exception e) // Обрабатываем универсальное исключение
        {
            return "error"; // Выводим страницу с ошибкой
        }

    }


    public String getChangePswPage(long id, Model model) {

        // Получаем пользователя по id и передаем в модель
        model.addAttribute("userAttribute",getUserById(id));

        // Выводим страницу редактирования пароля пользователя
        return "changepsw";
    }


    public String savePassword(User user) {

        try {
            repository.saveAndFlush(user); // Сохраняем пользователя через репозиторий Spring

            return "savedpsw"; // Выводим страницу успещного сохранения
        }
        catch (Exception e) // Обрабатываем универсальное исключение
        {
            return "error"; // Выводим страницу с ошибкой
        }
    }

    public String getDeletePage(long id, Model model) {

        // Получаем пользователя по id и передаем в модель
        model.addAttribute("userAttribute",getUserById(id));

        // Выводим страницу удаления пользователя
        return "delete";
    }

    public String delUser(User user) {

        try {
            repository.delete(user.getId()); // Удаляем пользователя по id через репозиторий Spring

            return "deleted"; // Выводим страницу успещного удаления
        }
        catch (Exception e) // Обрабатываем универсальное исключение
        {
            return "error"; // Выводим страницу с ошибкой
        }
    }

    public User getUserById(long id) {

        // Получаем пользователя по id

        User user = repository.findOne(id);

        return user;
    }


}
