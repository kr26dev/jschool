package ru.azoft.testtask.jschool.service;

import org.springframework.ui.Model;
import ru.azoft.testtask.jschool.entity.User;

/**
 * Created by Admin on 30.05.2017.
 */
public interface UserService {

String getAllUsers(Model model);

String getAddPage(Model model);

String saveNewUser(User user);

String getEditPage(long id, Model model);

String saveUser(User user);

String getChangePswPage(long id, Model model);

String savePassword(User user);

String getDeletePage(long id, Model model);

String delUser(User user);

User getUserById(long id);


}
