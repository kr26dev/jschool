package ru.azoft.testtask.jschool.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

/**
 * Created by Admin on 29.05.2017.
 */
@Configuration // означает, что данный класс ялвется конфигурацией, который нужно выполнить до того, как разворачивать контекст спринга
@EnableWebMvc // включение режима WebMVC, даст возможность использовать контроллеры и rest-контроллеры
@ComponentScan("ru.azoft.testtask.jschool") // где мы должны искать остальные пакеты

public class WebConfig extends WebMvcConfigurerAdapter {

    @Bean
    public UrlBasedViewResolver setupViewResolver() {
        UrlBasedViewResolver resolver = new UrlBasedViewResolver();
        // указываем где будут лежать наши веб-страницы
        resolver.setPrefix("/WEB-INF/");
        // формат View который мы будем использовать
        resolver.setSuffix(".jsp");
        resolver.setViewClass(JstlView.class);

        return resolver;
    }
}




