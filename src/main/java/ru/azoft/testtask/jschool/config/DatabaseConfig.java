package ru.azoft.testtask.jschool.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


/**
 * Created by Admin on 29.05.2017.
 */


@Configuration // класс-конфигурация
@EnableJpaRepositories("ru.azoft.testtask.jschool.repository") // подключаем JPA-репозитории
@EnableTransactionManagement // включаем Transaction Management
@PropertySource("classpath:db.properties") // где искать настройки БД
@ComponentScan("ru.azoft.testtask.jschool") // где находятся подключаемые классы

public class DatabaseConfig {


    @Resource
    private Environment env;

    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() { // Настройки Entity Manager
        // Entity manager - это интерфейс для доступа к БД

        // Сюда подгружаем настройки БД и настройки Hibernate

        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan(env.getRequiredProperty("db.entity.package"));

        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

        em.setJpaProperties(getHibernateProperties());

        return em;
    }

    private Properties getHibernateProperties() { // Настройки Hibernate

        try { // Загружаем данные из файла hibernate.properties (в корне)
            Properties properties = new Properties(); // новый объект для хранения настроек Hibernate
            // Загружаем все в поток "is"
            InputStream is = getClass().getClassLoader().getResourceAsStream("hibernate.properties");

            properties.load(is); // Загружаем из потока в объект настроек

            return properties; // Вовзращаем объект настроек

        } catch (IOException e) { // Если не получилось, выводим ошибку, что файл не найден

            throw new IllegalArgumentException("Can't find 'hibernate.properties' in classpath!", e);
        }
    }


    @Bean(name = "DataSource")
    public DataSource dataSource() { // Загружаем конфигурацию базы данных

        BasicDataSource ds = new BasicDataSource(); // Новый объект конфи-ии
        ds.setUrl(env.getRequiredProperty("db.url")); // Загружаем путь к БД
        ds.setDriverClassName(env.getRequiredProperty("db.driver")); // Загружаем драйвер
        ds.setUsername(env.getRequiredProperty("db.username")); // Загружаем имя пользователя
        ds.setPassword(env.getRequiredProperty("db.password")); // Загружаем пароль

        // Загружаем конфигурацию connection pool
        ds.setInitialSize(Integer.valueOf(env.getRequiredProperty("db.initialSize")));
        ds.setMinIdle(Integer.valueOf(env.getRequiredProperty("db.minIdle")));
        ds.setMaxIdle(Integer.valueOf(env.getRequiredProperty("db.maxIdle")));
        ds.setTimeBetweenEvictionRunsMillis(Long.valueOf(env.getRequiredProperty("db.timeBetweenEvictionRunsMillis")));
        ds.setMinEvictableIdleTimeMillis(Long.valueOf(env.getRequiredProperty("db.minEvictableIdleTimeMillis")));
        ds.setTestOnBorrow(Boolean.valueOf(env.getRequiredProperty("db.testOnBorrow")));
        ds.setValidationQuery(env.getRequiredProperty("db.validationQuery"));

        return ds; // Возвращаем объект конф-ии
    }

    @Bean(name = "transactionManager")
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager manager = new JpaTransactionManager();
        manager.setEntityManagerFactory(entityManagerFactory().getObject());

        return manager;
    }


}
