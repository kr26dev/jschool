<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSchool Web Panel</title>
</head>

<body>
<h2>JSchool Web Panel</h2>
<h3>Редактирование пользователя</h3>

<form:form modelAttribute="userAttribute" method="POST" action="/jschool/edit/${userAttribute.id}">
    <table>
        <tr>
            <td><form:label path="id">ID:</form:label></td>
            <td><form:input path="id" disabled="true"/></td>
        </tr>
        <tr>
            <td><form:label path="name">Имя:</form:label></td>
            <td><form:input path="name" maxlength="30"/></td>
        </tr>
        <tr>
            <td><form:label path="lastname">Фамилия:</form:label></td>
            <td><form:input path="lastname" maxlength="50"/></td>
        </tr>
        <tr>
            <td><form:label path="birthdate">Дата рождения:</form:label></td>
            <td><form:input path="birthdate" type="date" maxlength="10"/></td>
        </tr>
        <tr>
            <td><form:label path="login">Логин:</form:label></td>
            <td><form:input path="login" maxlength="50"/></td>
        </tr>
        <tr>
            <td><form:label path="password">Пароль:</form:label></td>
            <td><form:input path="password" type="password" maxlength="50"/></td>
        </tr>
        <tr>
            <td><form:label path="description">Описание:</form:label></td>
            <td><form:input path="description" maxlength="150"/></td>
        </tr>
        <tr>
            <td><form:label path="address">Адрес:</form:label></td>
            <td><form:input path="address" maxlength="150"/></td>
        </tr>
    </table>
    <br>
    <input type="submit" name="submit" value="Сохранить изменения" />
</form:form>
<br>
<c:url var="mainpageUrl" value="/jschool" />
<p><a href="${mainpageUrl}">Вернуться на главную страницу</a></p>
</body>
</html>