<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSchool Web Panel</title>
</head>

<body>
<h2>JSchool Web Panel</h2>
<h3>Удаление пользователя</h3>

<c:url var="deleteUrl" value="/jschool/deleteuser/${userAttribute.id}" />
<c:url var="mainpageUrl" value="/jschool" />
<p:p modelAttribute="userAttribute"><b>Вы действительно хотите удалить пользователя </b>${userAttribute.login}<b>?</b></p:p>

<p><a href="${deleteUrl}">Да, удалить пользователя</a></p>
<p><a href="${mainpageUrl}">Нет, вернуться на главную страницу</a></p>

</body>
</html>