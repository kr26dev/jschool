<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style><%@include file="styles/style.css"%></style>
    <title>JSchool Web Panel</title>
</head>

<body>
<h2>JSchool Web Panel</h2>

<c:url var="createUrl" value="/jschool/add" />
<p><a href="${createUrl}" style="font-size: 20px;">Создать пользователя</a></p>
<br>

<table class="simple-little-table" cellspacing='0'>
   <tr><th>ID</th>
       <th>Имя</th>
       <th>Фамилия</th>
       <th>Дата рождения</th>
       <th>Логин</th>
       <th>Описание</th>
       <th>Адрес</th>
    <c:forEach items="${users}" var="user">
        <c:url var="editUrl" value="/jschool/edit/${user.id}" />
        <c:url var="chpswUrl" value="/jschool/changepsw/${user.id}" />
        <c:url var="deleteUrl" value="/jschool/delete/${user.id}" />
    <tr>
        <td><c:out value="${user.id}"/></td>
        <td><c:out value="${user.name}"/></td>
        <td><c:out value="${user.lastname}"/></td>
        <td><c:out value="${user.birthdate}"/></td>
        <td><c:out value="${user.login}"/></td>
        <td><c:out value="${user.description}"/></td>
        <td><c:out value="${user.address}"/></td>
        <td><a href="${editUrl}">Редактировать</a></td>
        <td><a href="${chpswUrl}">Изменить пароль</a></td>
        <td><a href="${deleteUrl}">Удалить</a></td>
    </tr>
    </c:forEach>

</table>
</body>
</html>