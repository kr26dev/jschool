<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSchool Web Panel</title>
</head>

<b>
<h2>JSchool Web Panel</h2>
<h3>Изменение пароля пользователя</h3>
<p:p>Пользователь: ${userAttribute.login}</p:p>
<form:form modelAttribute="userAttribute" method="POST" action="/jschool/changepsw/${userAttribute.id}">

<table>
<form:input path="id" style="visibility:hidden"/>
<form:input path="name" style="visibility:hidden"/>
<form:input path="lastname" style="visibility:hidden"/>
<form:input path="birthdate" style="visibility:hidden"/>
<form:input path="login" style="visibility:hidden"/>
<br>
<form:label path="password">Пароль: </form:label>
<form:input path="password" type="password" size="50"/>

<form:input path="description" style="visibility:hidden"/>
<form:input path="address" style="visibility:hidden"/>

</table>

    <br>
    <input type="submit" name="submit" value="Изменить пароль" />
</form:form>
<br>
<c:url var="mainpageUrl" value="/jschool" />
<p><a href="${mainpageUrl}">Вернуться на главную страницу</a></p>

</body>
</html>