<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSchool Web Panel</title>
</head>

<body>
<h2>JSchool Web Panel</h2>
<h3>Пароль пользователя был успешно изменен!</h3>
<c:url var="mainpageUrl" value="/jschool" />
<p><a href="${mainpageUrl}">Вернуться на главную страницу</a></p>


</body>
</html>